#!/bin/bash

# script based on the modified answer from pawamoy:
# https://stackoverflow.com/a/47799826

snip_store=$"$HOME/.snip_store/"
echo "completion ${snip_store}"

_complete_specific_path() {
  # declare variables
  local _item _COMPREPLY _old_pwd

  # if we already are in the completed directory, skip this part
  if [ "${PWD}" != "$1" ]; then
    _old_pwd="${PWD}"
    # magic here: go the specific directory!
    pushd "$1" &>/dev/null || return

    # init completion and run _filedir inside specific directory
    _init_completion -s || return
    _filedir

    # iterate on original replies
    for _item in "${COMPREPLY[@]}"; do
      # this check seems complicated, but it handles the case
      # where you have files/dirs of the same name
      # in the current directory and in the completed one:
      # we want only one "/" appended
      if [ -d "${_item}" ] && [[ "${_item}" != */ ]] && [ ! -d "${_old_pwd}/${_item}" ]; then
        # append a slash if directory
        _COMPREPLY+=("${_item}/")
      else
        _COMPREPLY+=("${_item}")
      fi
    done

    # allow for space only when completing full paths
    # (i.e. edning with a file)
    if [ -f "${_COMPREPLY}" ]; then
	true	
    else
	compopt -o nospace
    fi

    # popd as early as possible
    popd &>/dev/null
 
    # set the values in the right COMPREPLY variable
    COMPREPLY=( "${_COMPREPLY[@]}" )

    # clean up
    unset _COMPREPLY
    unset _item
  else
    # we already are in the completed directory, easy
    _init_completion -s || return
    _filedir
  fi
}

_snip() {
  _complete_specific_path "$snip_store"
}

# -o bashdefault -o default 
complete -F _snip snip
