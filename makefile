# messages
install_msg = "to install, run: 'sudo make install' "
uninstall_msg = "to uninstall, run: 'sudo make uninstall' "
remove_snippets_folder = "to remove snippets folder run: make remove_snippets"

# define paths
home_bin_path = "/home/${SUDO_USER}/bin/snip"
# could use 'pkg-config --variable=completionsdir bash-completion'
# but don't now how to make it work
bash_completion_path = "/usr/share/bash-completion/completions/snip"

# user can remove the snip_store without sudo or with it
ifeq ($(SUDO_USER),)
snippets_store_path = "${HOME}/.snip_store"
else
snippets_store_path = "/home/${SUDO_USER}/.snip_store"
endif


info:
	@echo "$(install_msg)\n$(uninstall_msg)\n$(remove_snippets_folder)"

install:
	# installation requires sudo, because it adds a completion
	# script to /usr/share/bash_completion/completions
	if [ -z "$(SUDO_USER)" ]; then \
		echo "you forgot sudo; ${install_msg}"; \
		exit 1; \
	else \
		true; \
	fi
	
	# create snippets store only if doesn't exist yet
	if [ ! -d "$(snippets_store_path)" ]; then \
		mkdir "$(snippets_store_path)" ; \
		chown -R "$(SUDO_USER)" "$(snippets_store_path)" ; \
	fi

	cp ./snip "$(home_bin_path)"
	chown "$(SUDO_USER)" "$(home_bin_path)"
	@echo "copied snip to " $(home_bin_path)

	cp ./bash_completion "${bash_completion_path}"
	@echo "copied autocomplete rules to " $(bash_completion_path)


uninstall:
	# remove the installed files: the script and the completion rules
	if [ -z "$(SUDO_USER)" ]; then \
		echo "you forgot sudo; ${uninstall_msg}"; \
		exit 1; \
	else \
		true; \
	fi
	rm $(home_bin_path)
	rm $(bash_completion_path)
	@echo "removed up:\n$(home_bin_path)\n$(bash_completion_path)"

remove_snippets:
	@echo "removed snippets folder: " $(snippets_store_path)
	rm -r snippets_store_path
