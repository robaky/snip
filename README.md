snip - a minimalistic snippet manager, inspired by pass (passwordstore.org). Navigates and copies to clipboard contents of snippet files, which are stored in `~/.snip_store`. The files must be created and organized manually.

Dependencies:
* bash
* make
* xclip
* tree

Installation:
* using sudo: `sudo make install`
* manual:
    * `cp ./snip ~/bin`
    * `mv ./bash_completion ~/.bash_completion`
    * `mkdir ~/.snip_store`
